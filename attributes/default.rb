#
# Cookbook Name:: haproxy-data_bag
# Default:: default
#
# Copyright 2009, Opscode, Inc.
# Copyright 2013, Jung-In.Jung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

default['haproxy']['balance_algorithm'] = "roundrobin"
default['haproxy']['enable_admin'] = true
default['haproxy']['admin']['address_bind'] = "127.0.0.1"
default['haproxy']['admin']['port'] = 22002
default['haproxy']['admin']['user'] = "admin"
default['haproxy']['admin']['pass'] = "1234"
default['haproxy']['pid_file'] = "/var/run/haproxy.pid"
default['haproxy']['cfg_file'] = "/etc/haproxy/haproxy.cfg"

default['haproxy']['user'] = "haproxy"
default['haproxy']['group'] = "haproxy"

default['haproxy']['default_mode'] = "tcp"
default['haproxy']['timeout_connect'] = "10s"
default['haproxy']['timeout_client'] = "1m"
default['haproxy']['timeout_server'] = "1m"

default['haproxy']['member_max_connections'] = 100
