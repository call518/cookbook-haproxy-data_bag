name              "haproxy"
maintainer        "Jung-In.Jung"
maintainer_email  "call518@gmail.com"
license           "Apache 2.0"
description       "Installs and configures haproxy. This is customized from Opscode Comunity Cookbook. (http://community.opscode.com/cookbooks/haproxy)"
long_description  IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version           "0.1.0"

recipe "haproxy", "Installs & configures haproxy"

%w{ debian ubuntu }.each do |os|
  supports os
end

depends           "cpu", ">= 0.2.0"
