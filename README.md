Description
===========

Installs haproxy and prepares the configuration location by data bag.

Requirements
============

## Platform

* Ubuntu (10.04+ due to config option change)
* Debian (6.0+)

## Etc

* Cookbook Name: haproxy-data_bag (After download, rename dir)

Attributes
==========

* `default['haproxy']['balance_algorithm']`
  sets the load balancing algorithm; defaults to roundrobin.

* `default['haproxy']['enable_admin']`
  whether to enable the admin interface. default true. Listens on port 22002.

* `default['haproxy']['admin']['address_bind']`
  sets the address to bind the administrative interface on, 127.0.0.1 by default

* `default['haproxy']['admin']['port']`
  sets the port for the administrative interface, 22002 by default

* `default['haproxy']['pid_file']`
  the PID file of the haproxy process, used in the tuning recipe.

* `default['haproxy']['cfg_file']`
  set the config file for haproxy, /etc/haproxy/haproxy.cfg by default

* `default['haproxy']['user']`
  user that haproxy runs as

* `default['haproxy']['group']`
  group that haproxy runs as

* `default['haproxy']['member_max_connections']`
  in both configs, set the maxconn per member

Recipes
=======

## Visualization of process,

[Prezi] http://prezi.com/racciri3_btw/chef-haproxy-cookbook-by-data-bag/

## default

Sets up haproxy using statically defined configuration by data bag. To override the configuration, modify the templates/default/haproxy.cfg.erb file directly, or supply your own and override the cookbook and source by reopening the `template[/etc/haproxy/haproxy.cfg]` resource.

## tuning

Uses the community `cpu` cookbook `cpu_affinity` LWRP to set affinity for the haproxy process.

Usage
=====

## Example: data bag

### https.js (sample)

    {
      "id": "https",
      "frontend":
            {
                    "0.0.0.0": "443"
            },
      "backend":
            {
                    "172.21.81.130": "443",
                    "172.21.81.135": "443"
            }
    }

### data bag structure (sample)

    {data_bag}-{haproxy-host-A}-{http.js}
                               -{https.js}
                               -{ftp.js}
                               -{mysql.js}
              -{haproxy-host-B}-{https.js}
                               -{mysql.js}

Depends
=======

### CPU cookbook

- Chef cookbook to manage CPU related actions on linux.
- Linux 2.6+ tested on Ubuntu.
- cpu >= 0.2.0
- http://community.opscode.com/cookbooks/cpu

License and Author
==================

- Copyright 2009, Opscode, Inc.
- Copyright 2013, Jung-In.Jung
- Author: Joshua Timberman (<joshua@opscode.com>)
- Editor: JungJungIn (<call518@gmail.com>)

	
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0
		
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
